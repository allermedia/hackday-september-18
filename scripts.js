//Scripts
//listen to shake event
var shakeEvent = new Shake({threshold: 5});
shakeEvent.start();
window.addEventListener('shake', function(){
  //alert("Shaked");
  //Redirect user after 3 secs
  setTimeout(function(){
    var randomProfile = Math.round(Math.random());
    window.location.href = "profile"+randomProfile+".html";
  },500);
}, false);

//stop listening
function stopShake(){
  shakeEvent.stop();
}

//check if shake is supported or not.
if(!("ondevicemotion" in window)){alert("Not Supported");}
