### Hackathon September -18
An ideation workshop was arranged with the Treffit team in 2018, with the purpose to come up with unique concepts that would separate from other dating apps.  
One of the ideas was the following:
##### _"Shake and Date"_  
**Problem**  
Treffit users wants to have an engaging browsing experience and find a potential date.  
**Solution**  
A feature that in an engaging way helps users explore potential dates in their area.  
**Demo (POC)**: djuptho.com/dev/shake

![](https://uc22adb51aa7ecfb7cc15d8ad8df.previews.dropboxusercontent.com/p/thumb/AAOTu1wF95fa1VZPe7lszCP0EoSHY_L18a6Ce7gXtuMHdXjZ3WQmebboo1bEpU5rILVXm8NV_pc79VTEzU9S4m0ZLrd3pNdzgiO8eAbJKT5vUHhXTcqdeLq0iwRTLvxg3U1OEdSpw-TCqMxUBHz1Lxl2SARw0X6gW6DeuaPqt4Fc3--QojGqtCFXZ3A_KN4cpqkBQYiTfd0VebqUM-HpfHWtxZVzkl7sqimteLwki_ttIw/p.png?size=2048x1536&size_mode=3)
